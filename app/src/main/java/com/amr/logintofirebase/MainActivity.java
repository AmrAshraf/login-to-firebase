package com.amr.logintofirebase;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.ShareCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {


    FirebaseAuth auth;
    // FirebaseAuth.AuthStateListener authStateListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        InternetReciever.checkConnection(this);//check connection first time only and then broadcast will go
        Firebase.setAndroidContext(this);
        auth = FirebaseAuth.getInstance();

        /*authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() == null) {
                    Toast.makeText(MainActivity.this, "Wrong email or password", Toast.LENGTH_SHORT).show();
                } else {
                    startActivity(new Intent(MainActivity.this, AfterLogIn.class));
                }
            }
        };*/
        // auth.addAuthStateListener(authStateListener);
        final TextView email = (TextView) findViewById(R.id.email);
        final TextView password = (TextView) findViewById(R.id.password);
        final Button login = (Button) findViewById(R.id.login);
        final Button register = (Button) findViewById(R.id.register);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (InternetReciever.isConnected) {
                    if(email.getText().toString().isEmpty()||password.getText().toString().isEmpty())
                    {
                        Toast.makeText(MainActivity.this, "Empty email or password", Toast.LENGTH_SHORT).show();

                        return;
                    }
                    auth.signInWithEmailAndPassword(email.getText().toString(), password.getText().toString())
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                                       @Override
                                                       public void onComplete(@NonNull Task<AuthResult> task) {
                                                           if (!task.isSuccessful()) {
                                                               Toast.makeText(MainActivity.this, "Try Again", Toast.LENGTH_SHORT).show();
                                                           } else {
                                                               startActivity(new Intent(MainActivity.this, AfterLogIn.class));
                                                               finish();
                                                           }
                                                       }
                                                   }

                            );

                } else {
                    Toast.makeText(MainActivity.this, "There isn't a connection", Toast.LENGTH_SHORT).show();

                }
            }
        });
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (InternetReciever.isConnected) {
                    startActivity(new Intent(MainActivity.this, SignUp.class));
                    finish();
                } else {
                    Toast.makeText(MainActivity.this, "There isn't a connection", Toast.LENGTH_SHORT).show();

                }
            }
        });
    }
}
