package com.amr.logintofirebase;

import java.io.Serializable;

/**
 * Created by Amr on 10/7/2016.
 */
public class Data {

    public String name;
    public String address;
    public String phoneNumber;

    Data(String name,String address,String phoneNumber)
    {
        this.name=name;
        this.address=address;
        this.phoneNumber=phoneNumber;
    }

}
