package com.amr.logintofirebase;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by Amr on 10/5/2016.
 */
public class InternetReciever extends BroadcastReceiver {
    public static boolean isConnected=false;

    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        isConnected = (activeNetwork != null
                && activeNetwork.isConnectedOrConnecting());

        Log.d("app d",String.valueOf(isConnected));

    }
    public static void  checkConnection(Context context)
    {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        isConnected = (activeNetwork != null
                && activeNetwork.isConnectedOrConnecting());

        Log.d("app d",String.valueOf(isConnected));

    }
}
