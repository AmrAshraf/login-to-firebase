package com.amr.logintofirebase;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SignUp extends AppCompatActivity {
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        final TextView newEmail = (TextView) findViewById(R.id.newemail);
        final TextView newpassword = (TextView) findViewById(R.id.newpassword);
        final Button signup = (Button) findViewById(R.id.signup);
        final TextView name = (TextView) findViewById(R.id.name);
        final TextView address = (TextView) findViewById(R.id.address);
        final TextView phoneNumber = (TextView) findViewById(R.id.phoneNumber);
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!newEmail.getText().toString().isEmpty() ||
                        !newpassword.getText().toString().isEmpty() ||
                        !name.getText().toString().isEmpty() ||
                        !address.getText().toString().isEmpty() ||
                        !phoneNumber.getText().toString().isEmpty()) {
                    auth = FirebaseAuth.getInstance();
                    auth.createUserWithEmailAndPassword(newEmail.getText().toString(), newpassword.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (!task.isSuccessful()) {
                                Toast.makeText(SignUp.this, "Authentication failed." + task.getException(),
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                FirebaseDatabase database = FirebaseDatabase.getInstance();
                                DatabaseReference databaseReference = database.getReference();

                                Data data = new Data(name.toString(),address.toString(),phoneNumber.toString());
                                databaseReference.setValue(data);
                                startActivity(new Intent(SignUp.this, AfterLogIn.class));
                                finish();
                            }
                        }
                    });
                }
            }
        });

    }
}
